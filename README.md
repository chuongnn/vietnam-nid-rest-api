# HyperVerge Documents - API Documentation for Vietnam National ID

## Overview

This documentation describes hyperdocs API v1. If you have any queries please contact support. The postman collection will also be added later.

1. Schema
1. Parameters
1. Root Endpoint
1. Authentication
1. Media Types
3. Supported Endpoints
3. Supported id_types 
2. Optional Parameters


## Schema

Currently, for the prototype only HTTP is supported. All data is received as JSON, and all image uploads are to be performed as form-data (POST request). Incase of a pdf file input, the key name has to be `pdf` and in all other cases, the key name for the image could be anything apart from `pdf`.

## Parameters
All optional and compulsory parameters are passed as part of the request body.

## Root Endpoint
A `GET` request can be issued to the root endpoint to check for successful connection. Actual endpoint URL will be published later.

	 curl http://apac.docs.hyperverge.co/v1

The `plain/text` reponse of `"AoK!"` should be received.

## Authentication

Currently, a simple appId, appKey combination is passed in the request header. The appId and appKey are provided on request by the HyperVerge team. If you would like to try the API, please reach out to contact@hyperverge.co

	curl -X POST http://apac.docs.hyperverge.co/v1/readNID \
	  -H 'appid: xxx' \
	  -H 'appkey: yyy' \
	  -H 'content-type: multipart/form-data;' \
	  -F 'image=@abc.png' 


On failed attempt with invalid credentials or unauthorized access the following error message should be received :

	{
	  "status": "failure",
	  "statusCode": "401",
	  "error": {
	    "developerMessage": "unauthorized",
	  }
	}

Please do not expose the appid and appkey on browser applications. In case of a browser application, set up the API calls from the server side.

## Media Types

Currently, `jpeg, png and tiff` images and `pdf` are supported by the HyperDocs image extraction APIs. 

1. `/readNID` on an image.
	
		curl -X POST http://apac.docs.hyperverge.co/v1/readNID \
		  -H 'appid: xxx' \
		  -H 'appkey: yyyy' \
		  -H 'content-type: multipart/form-data;\
		  -F 'image=@image_path.png'

2. `/readNID` on a pdf.
	
		curl -X POST http://apac.docs.hyperverge.co/v1/readNID \
		  -H 'appid: xxx' \
		  -H 'appkey: yyyy' \
		  -H 'content-type: multipart/form-data;\
		  -F 'pdf=@image_path.pdf'


## Supported APIs



Can be used to extract information from any or one of the supported documents depending on the endpoint.

* **URL**

    - /readNID : used for any of the supported Vietnam National ID documents
  
* **Method:**

    `POST`

* **Header**
	
	- content-type : 'formdata'
	- appid 
	- appkey
	
* **Request Body**

	- image or pdf
	- conf - optional parameter, to be set to "yes" if needed.
  
* **Success Response:**

    * **Code:** 200 <br />
    * Incase of a properly made request, the response would follow schema.

		
		```
		{
			"status" : "success",
			"statusCode" : "200",
			"result" : <resultObject>
		}
		```
		
	    The `resultObject` has the following Schema : 

        ```
		[{
			details : {
				"field-1" : {
				    "value" : "extracted-value-1",
				    "conf" : <float-value>,
				    "to-be-reviewed" : "yes/no"
				},
				"field-2" : {
				    "value" : "extracted-value-2",
				    "conf" : <float-value>,
				    "to-be-reviewed" : "yes/no"
				},
				"field-3" : {
				    "value" : "extracted-value-3",
				    "conf" : <float-value>,
				    "to-be-reviewed" : "yes/no"
				},
				..
			},
			type : "id_type"
		}]
		```
	
* **Error Response:**

    There are 3 types of request errors and `HTTP Status Code 400` is returned in all 3 cases:

        1. No Image input

        {
          "status": "failure",
          "statusCode": "400",
          "error": "API call requires one input image"
        }

        2. More than 1 image input

        {
          "status": "failure",
          "statusCode": "400",
          "error": "API call handles only one input image"
        }

        3. Larger than allowed image input

        {
          "status": "failure",
          "statusCode": "400",
          "error": "image size cannot be greater than 6MB"
        }

	All error messages follow the same syntax with the statusCode and status also being a part of the response body, and `string` error message with the description of the error.
	
	**Server Errors**
	We try our best to avoid these errors, but if by chance they do occur the response code will be 5xx.


* **Sample Calls:**

 - readNID
    
    ```
    curl -X POST http://apac.docs.hyperverge.co/v1/readNID \
		  -H 'appid: xxx' \
		  -H 'appkey: yyyy' \
		  -H 'content-type: multipart/form-data;\
		  -F 'image=@image_path.png'
    ```
  

## Supported id_types

|Types|Fields|
---|---
|id_front| dob, id, name, province
|id_back| doi, province
|id_new_front| address, dob, doi, doe, gender, id, name, province
|id_new_back| doe, doi

## Optional parameters



| parameter | value| default| description |
|---|:---:|:---:|---|
| outputImageUrl| yes/no | no | If set to "yes" `string`, a signed temporary url be will generated. The output image will be cropped to contain only the region of interest in the image, the document will also be aligned.Strongly advice users to not set the parameter to true unless required. HyperVerge does not want to store user's data beyond the processing time. |
|conf|yes/no|no|If set to "yes"(`string`), a confidence score is returned as part of the output for all fields. For any field with key \<field-name> extracted from the document, the confidence score would be reported with the object corresponding to the key as  conf. The score would be a float value between 0 and 1. The optimal threshold for the confidence score is 0.75, if confidence is reported to be less than this threshold value, manual review might be required based on the use case.|


